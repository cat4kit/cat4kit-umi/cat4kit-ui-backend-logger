# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.db import models


class Logger(models.Model):
    job_mode = models.CharField(
        max_length=500, blank=False, null=False
    )  # it can be test or scheduler
    job_id = models.CharField(max_length=500, blank=False, null=False)
    job_name = models.CharField(max_length=500, blank=False, null=False)
    name = models.CharField(max_length=500, blank=False, null=False)
    email = models.CharField(max_length=500, blank=False, null=False)
    levelname = models.CharField(max_length=500, blank=False, null=False)
    msg = models.CharField(max_length=500, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    created = models.CharField(
        max_length=500, blank=False, null=False
    )  # logger datetimes are stored as strings
