# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from rest_framework import serializers

from .models import Logger


class LoggerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Logger
        fields = (
            "levelname",
            "msg",
            "created",
            "name",
            "created_at",
        )


class LoggerNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Logger
        fields = (
            "name",
            "created_at",
        )
