# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.urls import path

from . import api

urlpatterns = [
    path("logger_post/", api.logger_post, name="logger_post"),
    path(
        "logger_delete_object_by_id/",
        api.logger_delete_object_by_id,
        name="logger_delete_object_by_id",
    ),
    path("logger_get/<str:thing>/", api.logger_get, name="logger_get"),
    path("logger_get_name/", api.logger_get_name, name="logger_get_name"),
    path(
        "dashboard_basecard/",
        api.dashboard_basecard,
        name="dashboard_basecard",
    ),
]
