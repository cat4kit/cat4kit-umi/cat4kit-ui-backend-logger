# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from datetime import datetime

import pytz
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)

from .forms import LoggerForm
from .models import Logger
from .serializers import LoggerNameSerializer, LoggerSerializer


def querydict_to_dict(query_dict):
    data = {}
    for key in query_dict.keys():
        v = query_dict.getlist(key)
        if len(v) == 1:
            v = v[0]
        data[key] = v
    return data


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def logger_post(request):
    local_tz = pytz.timezone("Europe/Berlin")
    created_dt_formatted = datetime.fromtimestamp(
        float(request.data["created"])
    )
    created_dt_formatted = created_dt_formatted.astimezone(local_tz)
    # remember old state
    _mutable = request.data._mutable

    # set to mutable
    request.data._mutable = True

    # сhange the values you want
    request.data["created"] = created_dt_formatted.strftime(
        "%Y-%m-%dT%H:%M:%S.%fZ"
    )

    # set mutable flag back
    request.data._mutable = _mutable
    form = LoggerForm(request.data)
    if not form.is_valid():
        print(form.is_valid(), form.errors)
    if form.is_valid():
        form.save()

    return JsonResponse("success", safe=False)


@api_view(["DELETE"])
@authentication_classes([])  # Choose the appropriate authentication class
@permission_classes([])  # Adjust permissions as needed
def logger_delete_object_by_id(request):
    try:
        print(request.data.get("name"))
        name = request.data.get("name")
        # email = request.data.get("email")
        logs = Logger.objects.filter(name=name)
        logs.delete()
        return JsonResponse({"message": "success"})
    except ObjectDoesNotExist:
        return JsonResponse({"message": "Object not found"}, status=404)
    except Exception as e:
        JsonResponse({"message": str(e)}, status=500)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def logger_get(request, thing):
    print(thing)
    Logs = Logger.objects.filter(name=thing)
    serializer = LoggerSerializer(Logs, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def logger_get_name(request):
    Logs = Logger.objects.values("name").distinct()
    serializer = LoggerNameSerializer(
        Logs[::-1][:4], context={"request": request}, many=True
    )
    return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def dashboard_basecard(request):
    # finished = 0
    harvested = 0
    # harvesting = 0
    ingested = 0
    # ingesting = 0

    job_length = len(Logger.objects.values("name").distinct())
    Logs = Logger.objects.all()
    serializer = LoggerSerializer(Logs, many=True)
    for i in range(len(serializer.data)):
        # if "Job finished successfully!" in serializer.data[i]['msg']:
        #     finished += 1
        if "Harvesting datasets is finished" in serializer.data[i]["msg"]:
            harvested += 1
        # if "Harvesting datasets is started" in serializer.data[i]['msg']:
        #     harvesting += 1
        if "Ingesting catalogs is finished" in serializer.data[i]["msg"]:
            ingested += 1
        # if "Ingesting catalogs is started" in serializer.data[i]['msg']:
        #     ingesting += 1

    return JsonResponse(
        {
            "jobs": job_length,
            #  'finished' : finished,
            "harvested": harvested,
            #  'harvesting' : harvesting,
            "ingested": ingested,
            #  'ingesting' : ingesting,
        }
    )
