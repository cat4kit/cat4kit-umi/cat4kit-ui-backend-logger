<!--
SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

SPDX-License-Identifier: CC-BY-4.0
-->

(installation)=
# Installation

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of cat4kit-ui-backend-logger!
```


To install the `cat4kit-ui-backend-logger` package for your Django
project, you need to follow two steps:

1. {ref}`Install the package <install-package>`
2. {ref}`Add the app to your Django project <install-django-app>`


(install-package)=

## Installation from PyPi

The recommended way to install this package is via pip and PyPi via:

```bash
pip install cat4kit-ui-backend-logger
```

or, if you are using `pipenv`, via:

```bash
pipenv install cat4kit-ui-backend-logger
```

Or install it directly from
[the source code repository on Gitlab][source code repository] via:

```bash
pip install git+https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-logger.git
```

The latter should however only be done if you want to access the
development versions  (see {ref}`install-develop`).

(install-django-app)=

## Install the Django App for your project

To use the `cat4kit-ui-backend-logger` package in your Django project,
you need to add the app to your `INSTALLED_APPS`, configure your `urls.py`, run
the migration, add a login button in your templates. Here are the step-by-step
instructions:

1. Add the `cat4kit_ui_backend_logger` app to your `INSTALLED_APPS`
2. in your projects urlconf (see {setting}`ROOT_URLCONF`), add include
   {mod}`cat4kit_ui_backend_logger.urls` via::

   ```python
    from django.urls import include, path

    urlpatterns += [
        path("cat4kit-ui-backend-logger/", include("cat4kit_ui_backend_logger.urls")),
    ]
    ```
3. Run ``python manage.py migrate`` to add models to your database
4. Configure the app to your needs (see {ref}`configuration`) (you can also
   have a look into the `cat4kit_ui_backend_logger` folder in the
   [source code repository][source code repository] for a possible
   configuration).

If you need a deployment-ready django setup for an app like this, please
have a look at the following template:
https://codebase.helmholtz.cloud/hcdc/software-templates/django-docker-template

That's it!

[source code repository]: https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-logger

(install-develop)=
## Installation for development

Please head over to our
`contributing guide <contributing>`{.interpreted-text role="ref"} for
installation instruction for development.
