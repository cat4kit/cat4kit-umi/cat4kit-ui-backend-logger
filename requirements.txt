# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

Django==4.2.4
django-cors-headers==4.2.0
django-environ==0.10.0
djangorestframework==3.14.0
djangorestframework-simplejwt==5.2.2
drf-api-logger==1.1.14
six==1.16.0
psycopg2==2.9.7
